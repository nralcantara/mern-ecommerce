const express = require ("express")
const router = express.Router()
const User = require("../models/User") //to find and create a user
const bcrypt = require("bcryptjs") //to encrypt password
const jwt = require("jsonwebtoken") //generate token

//Register
router.post("/", (req, res) => {
	//Username must be greater than 8 characters
	if(req.body.username.length <8) return res.status(400).json({
		status: 400,
		message: "Username must be greater than 8 characters"
	})
	//Password must be greater than 8 characters
	if(req.body.password.length <8) return res.status(400).json({
		status: 400,
		message: "Password must be greater than 8 characters"
	})
	if(req.body.password2.length <8) return res.status(400).json({
		status: 400,
		message: "Password2 must be greater than 8 characters"
	})
	//Password should match
	if(req.body.password != req.body.password2) return res.status(400).json({
		status: 400,
		message: "Password does not match"
	})
	//Check if username already exists
	User.findOne({username: req.body.username}, (err, user) => {
		if(user) return res.status(400).json({
			status: 400,
			message: "Username already exists"
		})
		bcrypt.hash(req.body.password, 10, (err, hashedPassword) => {
			const user = new User()
			user.fullname = req.body.fullname
			user.username = req.body.username
			user.password = hashedPassword
			user.save()
			return res.json({status: 200, message: "Registered Successfully"})
		})
	}) //end of User.findOne
})

//LOGIN POST localhost:4000/users/login
router.post("/login", (req, res) => {
	User.findOne({username: req.body.username}, (err, user) => {
		if(err || !user) return res.status(400).json({
			status:400, 
			message: "No user found"
		})
		bcrypt.compare(req.body.password, user.password, (err, result) => {
			if(!result) {
				return res.status(401).json({
					auth: false,
					message: "Invalid credentials",
					token: null
				})
			} else {
				user = user.toObject()
				delete user.password
				let token = jwt.sign(user, 'b49-ecommerce', { expiresIn: '1hr'})
				return res.status(200).json({
					auth: true,
					message: "Logged in Successfully",
					user,
					token
				})
			}
		})
	})
})


module.exports = router